# -*- encoding: utf-8 -*-
$:.unshift File.expand_path("../lib", __FILE__)
require "crazyflie-zmq/version"

Gem::Specification.new do |s|
  s.name          = "crazyflie-zmq"
  s.version       = CrazyflieZMQ::VERSION
  s.authors       = [ "Stephane D'Alu" ]
  s.email         = [ "stephane.dalu@insa-lyon.fr" ]
  s.homepage      = "http://github.com/sdalu/crazyflie-zmq"
  s.summary       = "Control Crazyflie via ZMQ protocol"

  s.add_dependency 'ffi-rzmq'

  s.add_development_dependency "yard"
  s.add_development_dependency "rake"
  s.add_development_dependency "redcarpet"

  s.has_rdoc      = 'yard'

  s.license       = 'Apache-2.0'
  

  s.files         = %w[ LICENSE Gemfile crazyflie-zmq.gemspec ] + 
		     Dir['lib/**/*.rb'] 
end
