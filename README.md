
# Crazyflie ZMQ
Crazyflie ZMQ interface

## HOW-TO

The crazyflie ZMQ server must be running and accessible.

~~~sh
# On the computer where the crazy radio dongle is connected
./crazyflie-clients-python/bin/cfzmq --url tcp://* -d
~~~


A simple demo code is provided below:

~~~ruby
# Predefine some log blocks
#  (be carefull size is restricted to packet size, see crazyflie documentation)
LOG_BLOCKS = {
    :range => { :variables => [ 'ranging.distance0',
                                'ranging.distance1',
                                'ranging.distance2' ],
                :period    => 100 },
}

# Connect to ZMQ sockets
$cf = CrazyflieZMQ.new('tcp://_ip_address_')

# Connect to crazyflie E7E7E7E7E7 on channel 80
# and preconfigure with log blocks
$cf.connect('radio://0/80/1M/E7E7E7E7E7', log_blocks: LOG_BLOCKS)

# Adjust PID settings
$cf['posCtlPid','xKp']= 1.0  # Using group, name notation
$cf['posCtlPid.yKp'  ]= 1.0  # Using name dotted notation

# Start logging using preconfigured log block :range
# and automatically log to csv file format
$cf.log_start(:range, file: :csv) { |*a|
     # Optionnally do thing in realtime with log information
     puts a.inspect
}

# Wait 2 minutes
sleep(120)

# Stop logging of :range
$cf.log_stop(:range)

# Disconnect from drone
$cf.disconnect
~~~


## Credit
* Chroma team-project Inria, INSA Lyon, CITI Lab
